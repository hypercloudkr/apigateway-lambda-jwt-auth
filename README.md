## What is?

- API Gateway 인증 관리자용 JWT verify function

## How to deploy?

1. npm run build
2. npm run zip
3. function.zip 수동 배포

> why? - serverless 환경이 아니니 layer 분리 및 ci환경 구성은 불필요하다 판단.
