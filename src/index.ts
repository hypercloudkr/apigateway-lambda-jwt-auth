import * as jwt from 'jsonwebtoken';
import type { JwtPayload } from 'jsonwebtoken';

interface ApiGatewayEvent {
  type: 'TOKEN';
  authorizationToken: string;
  methodArn: string;
}

export const handler = async (event: ApiGatewayEvent) => {
  const authroization = event.authorizationToken;
  const [type, token] = authroization.split(' ');

  try {
    if (type !== 'Bearer') {
      throw new Error('no Bearer token');
    }

    const decodedData = jwt.verify(token, process.env.JWT_SECRET_KEY as string);

    return allowPolicy(event.methodArn, decodedData);
  } catch (error) {
    console.log('error :', error);
    return denyAllPolicy();
  }
};

function denyAllPolicy() {
  return {
    principalId: '*',
    policyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Action: '*',
          Effect: 'Deny',
          Resource: '*'
        }
      ]
    }
  };
}

function allowPolicy(methodArn: string, decodedData: string | JwtPayload) {
  return {
    principalId: 'apigateway.amazonaws.com',
    policyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: 'Allow',
          Resource: methodArn
        }
      ]
    },
    context: {
      user: JSON.stringify(decodedData)
    }
  };
}

export default handler;
